#include <iostream>
using namespace std;

#include "Utilities/Logger.hpp"
#include "Utilities/Menu.hpp"

#include "DataStructure/LinkedListTester.hpp"
#include "DataStructure/VectorTester.hpp"
#include "DataStructure/QueueTester.hpp"

int main()
{
    Logger::Setup( false );

    LinkedListTester testLinkedList( "test-results-linked-list.html" );
    testLinkedList.Start();

    VectorTester testVector( "test-results-vector.html" );
    testVector.Start();

    QueueTester testQueue( "test-results-queue.html" );
    testQueue.Start();

    Menu::Pause();
    Logger::Cleanup();

    return 0;
}
